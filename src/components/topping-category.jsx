import React, {Component} from 'react';
import {connect} from 'react-redux';


class ToppingSidebar extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

        this.props.setInitialId(this.props.builderData[0].id);
    }

    render() {
        const sideBarData = this.props.builderData;

        return (
            <div className="pizza__category-sidebar">
                {sideBarData.map(data => {

                    return (
                        <div onClick={() => this.props.setCategoryId(data.id) && this.props.setCategoryName(data.label, data.is_multiselect)} key={data.id}
                             className="pizza__category-label">
                            <div className="icon-label">{data.label}</div>
                        </div>
                    )
                })}
            </div>
        )
    }

}

//state we want to get
const mapStateToProps = state => {
    return {
        builderData: state.builder_data
    };
};

// action dispatch
const mapDispatchToProps = dispatch => {
    return {
        setInitialId: (id) => dispatch({
            type: 'setInitialId',
            payload: id
        }),

        setCategoryId: (id) => dispatch({
            type: 'setCategoryId',
            payload: id
        }),
        setCategoryName: (name ,isMulti) => dispatch({
            type: 'setCategoryName',
            payload: name,
            payload_isMulti: isMulti
        })
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ToppingSidebar);