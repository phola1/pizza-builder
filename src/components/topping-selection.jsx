import React, {Component} from 'react';
import {connect} from 'react-redux';
import api_url from "../constants/constants";


class ToppingSelection extends Component {
    constructor(props) {
        super(props);
    }

    setActiveClass = (item) => {
        // item.size === '' ? ((item === this.props.selectedSize) ? 'active pizza__topping' : 'pizza__topping') : (this.props.pizzaData[this.props.currentParent] === item ? 'active pizza__topping' : 'pizza__topping')
        // console.log(this.props.pizzaData);
        // let data;
        // const selectedData = this.props.pizzaData[`${this.props.currentParent}`] ? data = this.props.pizzaData[`${this.props.currentParent}`].data : this.props.pizzaData[`${this.props.currentParent}`];
        if (item.size === '') {
            if ((item === this.props.selectedSize)) {
                return 'active pizza__topping';
            } else {
                return 'pizza__topping'
            }
        } else {
            if (this.props.pizzaData[`${this.props.currentParent}`] === item) {
                return 'active pizza__topping';
            } else {
                return 'pizza__topping'
            }
        }

    };

    consoleService = () => {
        // console.log(this.props.pizzaData["from the field"]);
    }


    filterBySize = (item, index, data) => {

        if (item.size === this.props.selectedSize.label || item.size === '') {
            return (
                <div key={index}
                     onClick={(item.size === '' ? () => this.props.setSizeId(item) : () => {
                         this.props.setPizzaItems(item, data);
                     })}
                     className={this.setActiveClass(item)}>
                    {/*{console.log('pizzaData', this.props.pizzaData)}*/}
                    <div className="pizza__topping-image">
                        <img src={api_url.image_url + item.image.base_image}/>
                    </div>
                    <div className="pizza__topping-name">
                        {item.label}
                    </div>
                </div>
            )
        }

    };

    componentDidMount() {
        this.props.setSizeId(this.props.builderData[0].items[0].label);
    }


    // data.items.filter(item => (item.size === this.props.selectedSize) || (item.size === ''))

    render() {
        const selectionData = this.props.builderData;
        return (
            <div className="pizza__topping-selector">
                {
                    selectionData.map(data => {
                        return (
                            this.props.selectedId === data.id &&
                            //    pizza topping container
                            <div key={data.id} className="pizza__topping-container">
                                {
                                    data.items.map((item, index) => this.filterBySize(item, index, data))
                                }
                            </div>
                        )
                    })
                }
            </div>
        )
    }

}

//state we want to get
const mapStateToProps = state => {
    return {
        builderData: state.builder_data,
        selectedId: state.selectedId,
        selectedSize: state.selectedSize,
        currentParent: state.currentParent,
        pizzaData: state.pizzaData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        setSizeId: (size) => dispatch({
            type: 'setSize',
            payload: size
        }),
        setPizzaItems: (item, parentData) => dispatch({
            type: 'setPizzaData',
            payload: item,
            parentData
        })
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ToppingSelection);