import React, {Component} from 'react';
import {connect} from 'react-redux';


class PizzaViewer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            array: new Array(28)
        }
    }


    createToppings = (multiData) => {
        console.log(multiData);

        let data;
        let dataContainer;
        let toppingContainer;
        let spanImages;

        dataContainer = document.createElement('div');
        dataContainer.setAttribute('class', 'pizza__viewer-box');

        toppingContainer = document.createElement('div');
        toppingContainer.setAttribute('class', 'pizza__toppings');

        dataContainer.append(toppingContainer);

        for (let i = 0; i < this.state.array.length; i++) {
            const randomFunction = () => {
                return (Math.ceil(Math.random() * this.state.array.length)) + 'deg'
            };

            spanImages = document.createElement('span');
            spanImages.setAttribute('style', `transform: rotate(${randomFunction()})`);
            toppingContainer.append(spanImages);

            for (let j = 0; j < 2; j++) {
                data = document.createElement('img');
                data.setAttribute("src", `https://d1x3qzv6qxkltw.cloudfront.net${multiData.image.web_image}`);
                spanImages.appendChild(data);
            }

            document.querySelector('.pizza__viewer-container').appendChild(dataContainer);

        }


        // spanImages.appendChild(data);
        //
        // toppingContainer.append(spanImages);

    };

    componentDidMount() {
        console.log(this.state.array);
    }

    createPizza = () => {
        const data = this.props.pizzaData;

        const objKeys = Object.keys(data);

        const dataToReturn = objKeys.map((key, index) => {
            return (
                <div key={index} className='pizza__viewer-box'>
                    {!data[key].isMultiple ? (!data[key].positions.length ?
                        <img src={'https://d1fdothp89wqgm.cloudfront.net' + data[key].image.web_image}/> :
                        <img
                            src={'https://d1fdothp89wqgm.cloudfront.net' + data[key].positions[0].tooltip_positions[0].image}/>)
                        :
                        data[key].data.map((multiData, index) => {
                            return (
                                <div key={index}>
                                    {this.createToppings(multiData)}
                                </div>
                            )
                        })
                    }
                </div>
            )
        });

        return dataToReturn;

    }

    render() {
        console.log('pizzaData', this.props.pizzaData);

        return (
            <div className="pizza__viewer">

                <div className="pizza__viewer-container">
                    <div className='base__image'>
                        <img className="base_image"
                             src="https://d1x3qzv6qxkltw.cloudfront.net/assets/images/client/byp_plate.png"/>
                    </div>
                    {
                        this.createPizza()
                    }
                </div>


            </div>
        )
    }

}

//state we want to get
const mapStateToProps = state => {
    return {
        currentParent: state.currentParent,
        pizzaData: state.pizzaData
    };
};

export default connect(mapStateToProps, null)(PizzaViewer);