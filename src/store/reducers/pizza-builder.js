import uniq from 'lodash.uniq'

const initialState = {
    guest_token: localStorage.getItem('guest_token'),
    builder_data: null,
    selectedId: null,
    selectedSize: '7"',
    currentParent: null,
    prevParent: null,
    isMultiSelect: 0,
    modifiedData: null,
    modifiedMultiData: [],
    pizzaData: {}
};


const PizzaReducer = (state = initialState, action) => {

        if (action.type === 'setToken') {
            console.log(action);
            return {
                ...state,
                guest_token: action.payload
            }
        } else if (action.type === 'setBuilderData') {
            console.log(action);
            return {
                ...state,
                builder_data: action.payload
            }
        } else if (action.type === 'setInitialId') {
            console.log(action);
            return {
                ...state,
                selectedId: action.payload
            }
        } else if (action.type === 'setCategoryId') {
            console.log(action);
            return {
                ...state,
                selectedId: action.payload
            }
        } else if (action.type === 'setSize') {
            console.log(action);
            return {
                ...state,
                selectedSize: action.payload
            }
        } else if (action.type === 'setCategoryName') {
            console.log(action.payload);
            console.log(action.payload_isMulti);

            state.prevParent = state.currentParent;
            console.log(state.prevParent);

            return {
                ...state,
                currentParent: action.payload.replace(/\s/g, ''),
                isMultiSelect: action.payload_isMulti
            }
        } else if (action.type === 'setPizzaData') {

            const parentLabel = action.parentData.label;

            console.log(state.currentParent);
            if (state.isMultiSelect === 0) {
                state.modifiedData = {
                    ...state.modifiedData,
                    [state.currentParent]: action.payload
                }
            } else {


                state.modifiedMultiData.push(action.payload);

                state.modifiedMultiData = uniq(state.modifiedMultiData);

                console.log(state.modifiedData);

                state.modifiedData = {
                    ...state.modifiedData,
                    toppings: {
                        isMultiple: true,
                        data: [...state.modifiedMultiData]
                    }
                }

                //
                // if (state.pizzaData[state.prevParent].isMultiple) {
                //     state.modifiedMultiData = []
                // }

            }


            return {
                ...state,
                pizzaData: {
                    ...state.pizzaData,
                    ...state.modifiedData

                }
            }
        }
        return state;
    }
;

export default PizzaReducer;