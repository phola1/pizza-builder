import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

import App from './cockpit/app'

//Redux
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import PizzaReducer from "./store/reducers/pizza-builder";


const store = createStore(PizzaReducer);

ReactDOM.render(
    <Provider store={store}> <App/> </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// hot module replacement
// if (module.hot) {
//     module.hot.accept();
// }
serviceWorker.unregister();
