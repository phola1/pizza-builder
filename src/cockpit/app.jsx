import React, {Component} from 'react';
import '../scss/style.scss';
import {connect} from 'react-redux';
import axios from "axios";
import api_url from "../constants/constants";
import PizzaViewer from "../components/pizza-viewer";
import ToppingSidebar from "../components/topping-category";
import ToppingSelection from "../components/topping-selection";

class App extends Component {

    //constructor
    constructor(props) {
        super(props);

        this.state = {
            guest_token: null,
            guestApiParams: null,
            guestApiOptions: null
        }
    }


    //api constant setter
    ApiConstants = () => {

        const setConstant = new Promise((resolve, reject) => {
            const params = new URLSearchParams();
            params.append('platform', 'web');

            const options = {
                headers: {
                    'X-localization': 'en',
                    'X-location': 5 + '',
                    'X-restaurant': 1 + ''
                },
            };

            this.setState({
                guestApiParams: params,
                guestApiOptions: options
            });

            resolve();
        });


        return setConstant;

    };

    // Guest token Setter
    guestToken = () => {
        // promise to set constants first and then call api
        this.ApiConstants().then(() => {
                axios.post(`${api_url.api}api/auth/guest`, this.state.guestApiParams, this.state.guestApiOptions).then(
                    response => {
                        const guest_token = response.data.data.guest_token;

                        localStorage.setItem('guest_token', guest_token);
                        this.props.setToken(localStorage.getItem('guest_token'));
                    }
                );
            }
        );
    };


    pizzaBuilder = () => {

        this.ApiConstants().then(() => {

            const builderOptions = {
                headers: {
                    ...this.state.guestApiOptions.headers,
                    'Authorization': 'Guest ' + this.props.guestTkn
                }
            };

            axios.get(`${api_url.api}api/pizzaexp/restaurant/1`, builderOptions).then(
                response => {
                    const builderData = response.data.data.customizable_data;

                    let builderDataCustom = builderData.filter(data => data.has_child === 0);
                    const toppingsData = builderData.filter(data => data.has_child !== 0)[0].items.map(toppings => toppings);

                    builderDataCustom = [...builderDataCustom, ...toppingsData];

                    this.props.setBuilderData(builderDataCustom);

                }
            );
        });
    };

    // Hook When component mounts
    componentDidMount() {
        const current_token = localStorage.getItem('guest_token') ? localStorage.getItem('guest_token') : null;

        if (!current_token) {
            //Api Call
            this.guestToken();
            this.pizzaBuilder();
        } else {
            // call pizza builder Api
            this.pizzaBuilder();
        }
    }


    render() {
        return (
            this.props.builderData &&
            <div className="pizza__wrapper">
                <ToppingSidebar/>

                <PizzaViewer/>

                <ToppingSelection/>
            </div>

        )
    }


}

//state we want to get
const mapStateToProps = state => {
    return {
        guestTkn: state.guest_token,
        builderData: state.builder_data
    };
};

// action dispatch
const mapDispatchToProps = dispatch => {
    return {
        setToken: (token) => dispatch({
            type: 'setToken',
            payload: token
        }),

        setBuilderData: (builderData) => dispatch({
            type: 'setBuilderData',
            payload: builderData
        }),

    };
};


export default connect(mapStateToProps, mapDispatchToProps)(App);
